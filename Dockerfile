FROM waleedka/modern-deep-learning

# File Author / Maintainer
MAINTAINER Prajwal Shreyas

# install jupyter Kernel gateway
#RUN pip install jupyter_kernel_gateway
#RUN pip install dlib

# Copy the application folder inside the container
ADD /app_gun_detection /app_gun_detection

# Get pip to download and install requirements:
RUN pip install -r /app_gun_detection/requirements.txt

# Expose ports
EXPOSE 5018

# Set the default directory where CMD will execute
WORKDIR /app_gun_detection
CMD python3 gun_detection.py
