#Import the OpenCV and dlib libraries
import cv2
import sqlite3
import pandas as pd
import numpy as np
# initialise variables
import os
import tensorflow as tf
import time

# initialise variables
from flask import Flask, render_template, request, redirect, url_for, send_from_directory
from werkzeug import secure_filename
import boto3 # AWS Packages
from boto3.s3.transfer import S3Transfer # AWS Packages
import base64
from io import StringIO, BytesIO
from PIL import Image, ImageFile
import json
import logging

dirpath = os.getcwd()
dirpath = dirpath
os.chdir(dirpath)
from config import *


#Flask api initialise
app = Flask(__name__)

s3_transfer = S3Transfer(boto3.client('s3', region_name='us-east-1',aws_access_key_id = AWS_prajwal_credetials['aws_access_key_id'], aws_secret_access_key = AWS_prajwal_credetials['aws_secret_access_key']))

# This is the path to the upload directory
app.config['UPLOAD_FOLDER'] = 'uploads/'
# These are the extension that we are accepting to be uploaded
app.config['ALLOWED_EXTENSIONS'] = set(['txt'])#, 'jpg', 'jpeg'])

# ------------------------------------------------------------------------------- Gun detection helper functions ---------------------------------------------------------------------------------------------
label_file = 'retrained_labels.txt'
# load graph for gun detection
model_file = 'gun_detection_graph.pb'

def load_graph(model_file):
  graph = tf.Graph()
  graph_def = tf.GraphDef()

  with open(model_file, "rb") as f:
    graph_def.ParseFromString(f.read())
  with graph.as_default():
    tf.import_graph_def(graph_def)

  return graph

def load_labels(label_file):
  label = []
  proto_as_ascii_lines = tf.gfile.GFile(label_file).readlines()
  for l in proto_as_ascii_lines:
    label.append(l.rstrip())
  return label

graph = load_graph(model_file)


# For a given file, return whether it's an allowed type or not
def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in app.config['ALLOWED_EXTENSIONS']

def compress_img(size, filepath, size1, size2, size3, im):

        # conditions to compress image
        if size > size1 and size < size2:
            im.save(filepath, "JPEG", quality=30, optimize=True, progressive=True)

        elif size > size2 and size < size3:
            im.save(filepath, "JPEG", quality=50, optimize=True, progressive=True)

        else:
            im.save(filepath, "JPEG", quality=30, optimize=True, progressive=True)

        return 0


# API set up  ---------------------------------------------------------------------------------------------
@app.route('/guntest')
def guntest():
    return 'Gun Detection API working'

@app.route('/gun_detection', methods=['POST'])
def gun_detection():

    if request.method == 'POST':
        logging.warning('Start of Algo')
        result = 'Error in processing, please ensure the format of the file is as per the documentation.'
        # Get the name of the uploaded file
        logging.warning(request)

        size1 = 1000000
        size2 = 2000000
        size3 = 3000000

        try:

            if request.is_json == False: # this section is supporting image file processing

                file = request.files['file']

                if allowed_file(file.filename):
                    logging.warning(file.filename)

                    # Make the filename safe, remove unsupported chars
                    filename = secure_filename(file.filename)
                    filename = file.filename
                    filename_img = filename.rsplit('.', 1)[0] + '.jpeg'
                    filepath = 'uploads/' + filename_img
                    logging.warning(filename_img)
                    img_data_base64 = file.stream.read()

                    #write image with reduced quality
                    im = Image.open(BytesIO(base64.b64decode(img_data_base64)))
                    im.save(filepath, "JPEG", quality=100, optimize=True, progressive=True)
                    logging.warning('img_data write complete')

                else:
                    result = "Not one of the allowed file extensions. Please try another file."

            elif request.is_json:  # this section is image file in json format processing

                   # read image data or URL
                    img_json = pd.DataFrame(request.json)
                    logging.warning('dataframe read')
                    img_file_name= img_json['image_file_name'][0] #image file name
                    img_url_data = img_json['image_data'][0] #image data

                    filename_img = img_file_name + '.jpeg'
                    filepath = 'uploads/' + filename_img
                    print(filepath)
                    im = Image.open(BytesIO(base64.b64decode(img_url_data)))
                    #im = Image.open(BytesIO((img_url_data)))
                    print(im)
                    im.save(filepath, "JPEG", quality=100, optimize=True, progressive=True)
                    logging.warning('img_data write complete')


            '''Upload a image file to S3'''
            #s3_transfer.upload_file(filepath, 'api.data', str(filename_img))
            #logging.warning('image uploaded to S3')

            # read image from drive
            logging.warning(filepath)
            im = Image.open(filepath)
            size = os.stat(filepath).st_size

            #compress_img(size, filepath, size1, size2, size3, im)
            '''Call the subfunction which perform the task '''
            result = get_gun_detection(graph, filepath)
            result = json.dumps(result)
            os.remove(filepath)#remove the file
            logging.warning('File delete')
            logging.warning(result)


        except Exception as e:
               logging.warning(e)
               result = str(e) #'Error while processing the image.'

        return result


def read_tensor_from_image_file(file_name, input_height=299, input_width=299, input_mean=0, input_std=255):
      input_name = "file_reader"
      output_name = "normalized"
      file_reader = tf.read_file(file_name, input_name)
      if file_name.endswith(".png"):
        image_reader = tf.image.decode_png(file_reader, channels = 3,
                                           name='png_reader')
      elif file_name.endswith(".gif"):
        image_reader = tf.squeeze(tf.image.decode_gif(file_reader,
                                                      name='gif_reader'))
      elif file_name.endswith(".bmp"):
        image_reader = tf.image.decode_bmp(file_reader, name='bmp_reader')
      else:
        image_reader = tf.image.decode_jpeg(file_reader, channels = 3,
                                            name='jpeg_reader')
      float_caster = tf.cast(image_reader, tf.float32)
      dims_expander = tf.expand_dims(float_caster, 0);
      resized = tf.image.resize_bilinear(dims_expander, [input_height, input_width])
      normalized = tf.divide(tf.subtract(resized, [input_mean]), [input_std])
      sess = tf.Session()
      result = sess.run(normalized)

      return result

def get_gun_detection(graph, image_path):
    file_name = image_path
    input_height = 299
    input_width = 299
    input_mean = 128
    input_std = 128
    input_layer = "Mul"
    output_layer = "final_result"

    t = read_tensor_from_image_file(file_name,
                                    input_height=input_height,
                                    input_width=input_width,
                                    input_mean=input_mean,
                                    input_std=input_std)

    input_name = "import/" + input_layer
    output_name = "import/" + output_layer
    input_operation = graph.get_operation_by_name(input_name);
    output_operation = graph.get_operation_by_name(output_name);

    with tf.Session(graph=graph) as sess:
      start = time.time()
      results = sess.run(output_operation.outputs[0],
                        {input_operation.outputs[0]: t})
      end=time.time()
    results = np.squeeze(results)

    top_k = results.argsort()[-5:][::-1]
    labels = load_labels(label_file)

    print('\nEvaluation time (1-image): {:.3f}s\n'.format(end-start))

    # for i in top_k:
    #   print(labels[i], results[i])
    label = str(labels[0])
    confidence_gun = results[0]
    # to be logged into cloud watch
    logging.warning({"Label":str(labels[0]),"Confidence":str(results[0])})
    result_label = {"Label":str(labels[0]),"Confidence":str(results[0])}
    # if confidence_gun > confidence_threshold and label=='gun':
    #     result_label = {"result":"Gun"}
    # else:
    #     result_label = {"result":"No Gun"}

    return result_label



################### Main Function ##################################################################

if __name__ == '__main__':
   app.run(host="0.0.0.0", debug=False, port=5018)
